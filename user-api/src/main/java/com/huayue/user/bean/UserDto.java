package com.huayue.user.bean;

import java.io.Serializable;

public class UserDto implements Serializable {
    /**用户id*/
    private Integer id;
    /**用户工号*/
    private String no;
    /**用户姓名*/
    private String name;
    /**是否管理员*/
    public Boolean admin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
