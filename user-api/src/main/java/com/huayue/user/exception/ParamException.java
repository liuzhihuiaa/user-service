package com.huayue.user.exception;

public class ParamException extends Exception {
    public ParamException(String message) {
        super(message);
    }
}
