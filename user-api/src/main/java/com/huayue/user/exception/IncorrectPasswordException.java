package com.huayue.user.exception;

public class IncorrectPasswordException extends Exception  {
    public IncorrectPasswordException(String message) {
        super(message);
    }
}
