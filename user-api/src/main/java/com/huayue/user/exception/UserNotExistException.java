package com.huayue.user.exception;

public class UserNotExistException extends Exception {
    public UserNotExistException(String message) {
        super(message);
    }
}
