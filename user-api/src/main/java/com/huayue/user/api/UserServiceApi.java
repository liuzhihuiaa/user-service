package com.huayue.user.api;

import com.huayue.user.bean.UserDto;
import com.huayue.user.exception.ParamException;

import java.util.Collection;

public interface UserServiceApi {

    /**
     * 添加用户
     * @param name    姓名
     * @param no    工号
     * @param password   密码
     * @exception  Exception
     * @return
     */
    UserDto add(String name, String no, String password) throws Exception;

    /**
     * 根据用户工号批量查询
     * @param noList  工号集合
     * @return
     */
    Collection<UserDto> queryByNo(Collection<String> noList) throws ParamException;

    /**
     * 根据工号查询用户
     * @param no  工号
     * @return
     */
    UserDto queryByNo(String no) throws ParamException;

    /**
     * 根据id批量查询用户
     * @param idList   用户id集合
     * @return
     */
    Collection<UserDto> queryById(Collection<Integer> idList) throws ParamException;

    /**
     * 根据id查询用户
     * @param id   用户id
     * @return
     */
    UserDto queryById(Integer id) throws ParamException;

    /**
     * 根据用户id删除用户
     * @param id   用户id
     * @return
     */
    UserDto removeById(Integer id)  throws Exception;

    /**
     * 根据用户工号删除用户
     * @param no   工号
     * @return
     */
    UserDto removeByNo(String no) throws Exception;

    /**
     * 根据id更新用户信息，不包含密码
     * @param user   用户信息
     * @return
     */
    UserDto update(UserDto user) throws Exception;

    /**
     * 根据用户id修改密码
     * @param id     用户id
     * @param password    密码
     * @return
     */
    boolean updatePasswordById(Integer id, String password) throws Exception;

    /**
     * 根据用户工号修改密码
     * @param no    工号
     * @param password    密码
     * @return
     */
    boolean updatePasswordByNo(String no, String password) throws Exception;

    /**
     * 校验身份
     * @param no  工号
     * @param password  密码
     * @return
     */
    UserDto verifyIdentity(String no, String password) throws Exception;

}
