package com.huayue.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huayue.user.bean.UserPo;

public interface UserMapper extends BaseMapper<UserPo> {
}
